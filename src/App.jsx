import './App.css'
import HomePage from './pages/Home/HomePage';
import Footer from "./components/Footer";

function App() {

  return (
    <>
      <HomePage />
      <Footer />
    </>
  );
}

export default App
