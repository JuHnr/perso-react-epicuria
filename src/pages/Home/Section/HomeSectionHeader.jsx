import Tag from "../../../components/Tag";
import "./../../../assets/styles/HomeSectionHeader.css";

function HomeSectionHeader(props) {
  const { tag, title, description} = props;

  return (
    <div className="HomeSectionHeader">
      <Tag tag={tag.toUpperCase()} />
      <h2 className="HomeSectionHeader-title">{title}</h2>
      <p className="HomeSectionHeader-description">{description}</p>
    </div>
  );
}

export default HomeSectionHeader;
