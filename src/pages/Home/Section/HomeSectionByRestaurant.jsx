import RestaurantCard from "../../../components/RestaurantCard";
import "./../../../assets/styles/HomeSectionByRestaurant.css";
import { Link } from "react-router-dom";

// Importe les images avec les chemins réels
import italian1 from "./../../../assets/italian1.jpeg";
import italian2 from "./../../../assets/italian2.jpeg";
import italian3 from "./../../../assets/italian3.jpeg";
import italian4 from "./../../../assets/italian4.jpeg";
import asian1 from "./../../../assets/asian1.jpg";
import asian2 from "./../../../assets/asian2.jpg";
import asian3 from "./../../../assets/asian3.jpg";
import asian4 from "./../../../assets/asian4.jpg";
import american1 from "./../../../assets/american1.jpg";
import american2 from "./../../../assets/american2.jpg";
import american3 from "./../../../assets/american3.jpg";
import american4 from "./../../../assets/american4.jpg";
import best1 from "./../../../assets/best1.jpg";
import best2 from "./../../../assets/best2.jpg";
import best3 from "./../../../assets/best3.jpg";
import best4 from "./../../../assets/best4.jpg";

const imageMap = {
  italian1: italian1,
  italian2: italian2,
  italian3: italian3,
  italian4: italian4,
  asian1: asian1,
  asian2: asian2,
  asian3: asian3,
  asian4: asian4,
  american1: american1,
  american2: american2,
  american3: american3,
  american4: american4,
  best1: best1,
  best2: best2,
  best3: best3,
  best4: best4,
};

function HomeSectionByRestaurant(props) {
  const { restaurantList } = props;

  return (
    <div className="HomeSectionByRestaurant-container">
      {restaurantList.map((restaurant) => (
        <Link to={`/restaurants/${restaurant.id}`} key={restaurant.id}>
          <RestaurantCard
            id={restaurant.id}
            pathImage={imageMap[restaurant.pathImage]}
            rating={restaurant.rating}
            title={restaurant.title}
            time={restaurant.time}
            tag={restaurant.tag}
          />
        </Link>
      ))}
    </div>
  );
}

export default HomeSectionByRestaurant;
