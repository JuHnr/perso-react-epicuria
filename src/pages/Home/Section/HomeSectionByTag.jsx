import TagCard from "../../../components/TagCard";
import "./../../../assets/styles/HomeSectionByTag.css";
import { Link } from "react-router-dom";

// Importe les images avec les chemins réels
import american from "./../../../assets/category-american.svg";
import japanese from "./../../../assets/category-japenese.svg";
import french from "./../../../assets/category-french.svg";
import italian from "./../../../assets/category-italian.svg";
import mexican from "./../../../assets/category-mexican.svg";
import plus from "./../../../assets/plus.svg";

const imageMap = {
  american: american,
  japanese: japanese,
  italian: italian,
  french: french,
  mexican: mexican,
};

function HomeSectionByTag(props) {
  const { tagList } = props;

  return (
    <div className="HomeSectionByTag-container">
      {tagList.map((tag) => (
        <Link to={`/categories/${tag.id}`} key={tag.id}>
          <TagCard
            id={tag.id}
            counter={tag.counter}
            pathImage={imageMap[tag.pathImage]}
            title={tag.title}
            description={tag.description}
          />
        </Link>
      ))}
      <Link to="/categories">
        <TagCard
          counter="21"
          pathImage={plus}
          title="Explorer"
          description="Toutes les catégories"
        />
      </Link>
    </div>
  );
}

export default HomeSectionByTag;
