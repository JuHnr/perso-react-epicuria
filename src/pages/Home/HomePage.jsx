import HomeHero from "./Hero/HomeHero";
import HomeSectionByTag from "./Section/HomeSectionByTag";
import HomeSectionByRestaurant from "./Section/HomeSectionByRestaurant";
import HomeSectionHeader from "./Section/HomeSectionHeader";
import "./../../assets/styles/HomePage.css";
import data from "./../../data/data.json";



function HomePage() {

  const tagList = data.tags;
  const italianRestaurantList = data.restaurants.italian;
  const asianRestaurantList = data.restaurants.asian;
  const americanRestaurantList = data.restaurants.american;
  const bestRestaurantList = data.restaurants.best;


  return (
    <main className="HomePage">
      <HomeHero />

      <section className="HomePageSection">
        <HomeSectionHeader tag="Catégories" title="" description="Les catégories les plus recherchées" />
        <HomeSectionByTag tagList={tagList} />
      </section>

      <section className="HomePageSection">
        <HomeSectionHeader
          tag="Favoris"
          title="Les mieux notés"
          description="Une sélection pour épater vos papilles"
        />
        <HomeSectionByRestaurant restaurantList={bestRestaurantList} />
      </section>

      <section className="HomePageSection">
        <HomeSectionHeader
          tag="Italie"
          title="Savourez la Dolce Vita"
          description="La tradition culinaire italienne chez vous avec des spécialités qui sentent bon le soleil"
        />
        <HomeSectionByRestaurant restaurantList={italianRestaurantList} />
      </section>

      <section className="HomePageSection">
        <HomeSectionHeader
          tag="Asie"
          title="Partez en Orient"
          description="Des plats authentiques aux saveurs umami : sushi et autre ramens pour des repas sains et surprenants"
        />
        <HomeSectionByRestaurant restaurantList={asianRestaurantList} />
      </section>

      <section className="HomePageSection">
        <HomeSectionHeader
          tag="USA"
          title="Direction les USA"
          description="Réconfortez-vous avec de délicieux plats américains"
        />
        <HomeSectionByRestaurant restaurantList={americanRestaurantList} />
      </section>
    </main>
  );
}

export default HomePage;
