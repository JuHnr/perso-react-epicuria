import HeroImage from "./../../../assets/header-hero.png";
import Header from "./HomeHeroHeader";
import './../../../assets/styles/HomeHero.css'
import SearchBar from "../../../components/SearchBar";
import LocationIcon from "./../../../assets/location-icon.svg"

function Hero(props) {

  return (
      <section className="Hero">
          
      <Header/>

      <div className="Hero-content">
        <div className="Hero-text">
          <h2>Faites voyager vos papilles</h2>
                  <h1>Toutes les saveurs du monde, directement chez vous</h1>
                  <SearchBar placeholder="Montpellier" optionalIcon={LocationIcon} />
        </div>
              <img src={HeroImage} alt="background image" />
      </div>
    </section>
  );
}

export default Hero;
