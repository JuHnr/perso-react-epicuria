import SearchBar from "../../../components/SearchBar";
import './../../../assets/styles/HomeHeroHeader.css'

function HomeHeroHeader() {

  return (
    <header className="HomeHeroHeader">
      <h1>Epicuria</h1>
      <SearchBar placeholder="Rechercher un restaurant, un plat..." />
    </header>
  );
}

export default HomeHeroHeader;
