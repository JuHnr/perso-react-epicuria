import Rating from "../../components/Rating";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import data from "./../../data/data.json";
import { useParams } from "react-router";
import { useEffect, useState } from "react";
import "./../../assets/styles/RestaurantDetails.css";
import MenuCard from "../../components/MenuCard";
import menuHotCaketerias from "./../../assets//menu-hot-caketerias.jpeg";
import menuDesayunos from "./../../assets//menu-desayunos.jpeg";
import menuOtros from "./../../assets//menu-otros-desayunos.jpeg";
import locationIcon from "./../../assets/green-location.svg";
import mealIcon from "./../../assets/green-meal.svg";
import priceIcon from "./../../assets/green-price.svg";
import timeIcon from "./../../assets/green-time.svg";
import cookIcon from "./../../assets/green-cook.svg";
import aboutIcon from "./../../assets/green-about.svg";

import best2 from "./../../assets/best2.jpg";

const imageMap = {
  best2: best2,
};

function RestaurantDetails(props) {
  const { title, pathImage, rating, location, time, tag, price } = props;

  const { id } = useParams();
  const [restaurant, setRestaurant] = useState(null);

  useEffect(() => {
    //cible le restaurant grâce à l'id récupéré de l'url (les deux sont des strings)
    const selectedRestaurant = data.restaurants.best.find(
      (restaurant) => restaurant.id.toString() === id
    );

    //récupère et stocke le restaurant dans le state si n'est pas undefined
    if (selectedRestaurant) {
      setRestaurant(selectedRestaurant);
    }
  }, [id]);

  return (
    <>
      <Header />
      <article className="RestaurantDetails">
        {restaurant !== null ? (
          <>
            <header className="RestaurantDetails-header">
              <div className="RestaurantDetails-imageContainer">
                <img
                  src={imageMap[restaurant.pathImage]}
                  alt={restaurant.title}
                  className="RestaurantDetails-image"
                />
              </div>

              <div className="RestaurantDetails-infos">
                <div className="RestaurantDetails-infos_title">
                  <h1>{restaurant.title}</h1>
                  <Rating rating={restaurant.rating} />
                </div>

                <div className="RestaurantDetails-flexContent">
                  <img src={locationIcon} alt="location icon" />
                  <p>{restaurant.location}</p>
                </div>

                <div className="RestaurantDetails-flexContent">
                  <img src={timeIcon} alt="time icon" />
                  <p>{restaurant.time}</p>
                </div>

                <div className="RestaurantDetails-flexContent">
                  <img src={cookIcon} alt="cooking origin icon" />
                  <p>{restaurant.tag}</p>
                </div>

                <div className="RestaurantDetails-flexContent">
                  <img src={priceIcon} alt="price icon" />
                  <p>Prix moyen : {restaurant.price} €</p>
                </div>

                <div className="RestaurantDetails-flexContent">
                  <img src={aboutIcon} alt="about icon" />
                  <p>
                    <i>
                      "Venez découvrir notre carte de plats traditionnels
                      français."
                    </i>
                  </p>
                </div>
              </div>
            </header>

            <section>
              <div className="RestaurantDetails-flexContent RestaurantDetails-Menu_title">
                <img src={mealIcon} alt="meal icon" />
                <h2>A la carte</h2>
              </div>

              <div className="RestaurantDetails-MenuCardsContainer">
                <MenuCard
                  img={menuHotCaketerias}
                  alt="Brunch"
                  tag="Américain"
                  type="Petit-déjeuner"
                  title="Brunch américain"
                  description="Pancakes, toasts, boissons chaudes et autres gourmandises"
                  price="16"
                  rate="9.8"
                />

                <MenuCard
                  img={menuDesayunos}
                  alt="Planche"
                  tag="Continental"
                  type="Entrée"
                  title="Planche à partager"
                  description="Fromages et charcuteries pour 4 à 6 personnes"
                  price="18"
                  rate="9.8"
                />

                <MenuCard
                  img={menuOtros}
                  alt="Plat"
                  type="Plat principal"
                  tag="Français"
                  title="Omelette au fromage"
                  description="La traditionnelle omelette au fromage"
                  price="11"
                  rate="9.8"
                />

                <MenuCard
                  img={menuHotCaketerias}
                  alt="Brunch"
                  tag="Américain"
                  type="Petit-déjeuner"
                  title="Brunch américain"
                  description="Pancakes, toasts, boissons chaudes et autres gourmandises"
                  price="16"
                  rate="9.8"
                />

                <MenuCard
                  img={menuDesayunos}
                  alt="Planche"
                  tag="Continental"
                  type="Entrée"
                  title="Planche à partager"
                  description="Fromages et charcuteries pour 4 à 6 personnes"
                  price="18"
                  rate="9.8"
                />

                <MenuCard
                  img={menuOtros}
                  alt="Plat"
                  type="Plat principal"
                  tag="Français"
                  title="Omelette au fromage"
                  description="La traditionnelle omelette au fromage"
                  price="11"
                  rate="9.8"
                />
              </div>
            </section>
          </>
        ) : (
          <div>Chargement...</div>
        )}
      </article>
      <Footer />
    </>
  );
}

export default RestaurantDetails;
