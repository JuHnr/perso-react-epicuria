import React from 'react'
import ReactDOM from 'react-dom/client'
import App from './App.jsx'
import CategoryPage from './pages/CategoryPage/CategoryPage.jsx'
import AllCategories from './pages/CategoryPage/AllCategories.jsx'
import RestaurantDetails from './pages/RestaurantDetails/RestaurantDetails.jsx'
import './index.css'
import { createBrowserRouter, RouterProvider } from "react-router-dom";


const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
  },
  {
    path: "/restaurants/:id",
    element: <RestaurantDetails />,
  },
  {
    path: "/categories/:id",
    element: <CategoryPage />,
  },
  {
    path: "/categories",
    element: <AllCategories />,
  },
  {
    path: "/Search",
    element: <div>Search</div>,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>
);
