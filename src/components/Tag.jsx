import "./../assets/styles/Tag.css"

function Tag(props) {

    const { tag } = props;
    
  return (
    <div className="Tag-container">
      <h3 className="Tag-content">{tag}</h3>
    </div>
  );
}

export default Tag;
