import "./../assets/styles/Footer.css"
import Instagram from "./../assets/instagram.svg";
import Facebook from "./../assets/facebook.svg";
import Twitter from "./../assets/twitter.svg";
import Pinterest from "./../assets/pinterest.svg";


function Footer() {
  return (
    <footer className="Footer">
      <div className="Footer-infos">
        <p>
          Ce site est un projet personnel créé à des fins de démonstration et
          n'a pas vocation à être utilisé. L'entièreté de son contenu est
          fictif.
        </p>
        <p>
          <b>©JuHnr 2024 - Tous droits réservés.</b>
        </p>
      </div>
      <div className="Footer-social">
        <p>Suivez-nous !</p>
        <div className="Footer-social_logo-container">
          <img src={Instagram} alt="Instagram" className="instagram" />
          <img src={Facebook} alt="Instagram" />
          <img src={Twitter} alt="Instagram" />
          <img src={Pinterest} alt="Instagram" />
        </div>
      </div>
    </footer>
  );
}

export default Footer;
