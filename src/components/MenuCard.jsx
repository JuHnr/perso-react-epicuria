import "./../assets/styles/MenuCard.css";

function MenuCard(props) {
  const { img, alt, tag, title, description, type, price} = props;

  return (
    <div className="MenuCard_card">
      <div className="MenuCard_image-container">
        <img src={img} alt={alt} />
      </div>

      <div className="MenuCard_info">
        <h3> {type}</h3>
        <h2> {title}</h2>
        <p className="MenuCard_light-text"> {description}</p>
        <div className="MenuCard_sub-info">
          <p>{price} €</p>
          <p className="MenuCard_light-text"> {tag} </p>
        </div>
      </div>
    </div>
  );
}

export default MenuCard;
