import "./../assets/styles/TagCard.css";
import BadgeIndicator from "./BadgeIndicator";

function TagCard(props) {
  const { counter, pathImage, title, description } = props;

  return (
    <div className="TagCard">
      <div className="TagCard-imageContainer">
        <img src={pathImage} alt={title} />
        <BadgeIndicator counter={counter} />
      </div>
      <div className="TagCard-content">
        <h2>{title}</h2>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default TagCard;
