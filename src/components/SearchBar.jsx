import "./../assets/styles/SearchBar.css";
import SearchIcon from "./../assets/search-icon.svg";

function SearchBar(props) {
  
  const { placeholder, optionalIcon } = props;

  return (
    <div className="SearchBar">
      {optionalIcon && (
        <div className="optional-icon">
          <img src={optionalIcon} alt="icon" />
        </div>
      )}
      <input
        type="search"
        name="search"
        id="search"
        className="SearchBar-input"
        placeholder={placeholder}
      />
      <button type="submit" className="SearchBar-button">
        <img src={SearchIcon} alt="search icon" />
      </button>
    </div>
  );
}

export default SearchBar;
