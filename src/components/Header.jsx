import { Link } from "react-router-dom";
import "./../assets/styles/Header.css";
import SearchBar from "./SearchBar";

function Header() {
  return (
    <header className="Header">
      <Link to="/">
        <h1>Epicuria</h1>
      </Link>
      <SearchBar placeholder = "Rechercher..."/>
    </header>
  );
}

export default Header;
