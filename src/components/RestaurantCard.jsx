import "./../assets/styles/RestaurantCard.css";
import Rating from "./Rating";

function RestaurantCard(props) {
  const { pathImage, rating, title, time, tag } = props;
  return (
    <div className="RestaurantCard">
      <div className="RestaurantCard-image-container">
        <img src={pathImage} alt={title} />
      </div>

      <Rating rating={rating} />

      <div className="RestaurantCard-content">
        <h2 className="RestaurantCard-content_title">{title}</h2>
        <div className="RestaurantCard-content_info">
          <p>{time}</p>
          <p className="RestaurantCard-content_info--light">{tag}</p>
        </div>
      </div>
    </div>
  );
}

export default RestaurantCard;
