import "./../assets/styles/Rating.css";
import star from "../assets/star.png";

function Rating(props) {
    const { rating } = props;
  return (
    <div className="Rating">
      <img className="Rating-icon" src={star} alt="star"/>
      <p>{rating.toFixed(1)}</p>
    </div>
  );
}

export default Rating;
