import "./../assets/styles/BadgeIndicator.css"

function BadgeIndicator(props) {
  const { counter } = props;

  return <div className="BadgeIndicator">{counter}</div>;
}

export default BadgeIndicator;
